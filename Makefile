CFLAGS  = -std=c89 -pedantic -Wall -Wextra -D_POSIX_C_SOURCE=200809L
LDFLAGS = -lcursesw -lm
PREFIX  = /usr/local

tree2: main.c
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $<

clean:
	rm tree2

install:
	mkdir -p       $(DESTDIR)$(PREFIX)/bin
	cp    -f tree2 $(DESTDIR)$(PREFIX)/bin
	chmod 755      $(DESTDIR)$(PREFIX)/bin/tree2
