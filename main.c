/* This code is public domain.
 * I don't care what you do, just don't blame me.
 */

#include <ctype.h>
#include <curses.h>
#include <locale.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct line
{
    char         txt[BUFSIZ];
    struct line *next;
};

struct paneview
{
    struct line *lines;
    WINDOW      *win;
    int          focus;
    ssize_t      pos;
    ssize_t      top;
    ssize_t      end;
};

struct lineview
{
    char     txt[BUFSIZ];
    WINDOW  *win;
    ssize_t  pos;
};

struct dirview
{
    char            *cwd;
    struct paneview  tree;
    struct lineview  pick;
    ssize_t          depth;
};

struct topview
{
    struct dirview  dira;
    struct dirview  dirb;
    struct lineview cmd;
};

static void
append2tree(struct dirview *dv, char *txt)
{
    struct line *l;

    if (!dv->tree.lines)
        dv->tree.lines = calloc(1, sizeof(*dv->tree.lines));

    l = dv->tree.lines;
    for (; l->next; )
        l = l->next;

    if (l->txt[0])
        l = l->next = calloc(1, sizeof(*l));

    strncpy(l->txt, txt, sizeof(dv->tree.lines->txt));
}

static FILE *
popentree(char *cwd, int fullpath, int depth)
{
    char  cmd[BUFSIZ] = {0};
    char *popt;

    if (!cwd || (depth < 1))
        return 0;

    popt = "";
    if (fullpath)
        popt = "-fi";
    snprintf(cmd, (sizeof(cmd) - 1), "tree -L %d --noreport %s %s", depth, popt, cwd);

    return popen(cmd, "r");
}

static void
readpick(struct dirview *dv, char *env)
{
    FILE *fp;
    char  buf[BUFSIZ] = {0};
    int   i;
    int   n;

    n = dv->tree.pos;
    if (n < 0)
        return;

    fp = popentree(dv->cwd, 1, dv->depth);
    for (i = 0; i <= n; i++)
        fgets(buf, (sizeof(buf) - 1), fp);
    pclose(fp);

    strtok(buf, "\n");
    setenv(env, buf, 1);
    strncpy(dv->pick.txt, buf, (sizeof(dv->pick.txt) - 1));
}

static void
cleartree(struct paneview *tree)
{
    struct line *l;
    struct line *n;

    if (!tree | !tree->lines)
        return;

    for (l = tree->lines; l; l = n) {
        n = l->next;
        free(l);
    }
    tree->lines = 0;
}

static void
readtree(struct dirview *dv)
{
    char  buf[BUFSIZ] = {0};
    FILE *fp;
    int   i;

    cleartree(&dv->tree);

    fp = popentree(dv->cwd, 0, dv->depth);
    for (i = 0; fgets(buf, (sizeof(buf) - 1), fp); i++) {
        strtok(buf, "\n");
        append2tree(dv, buf);
    }
    dv->tree.end = i - 1;
    pclose(fp);
}

static void
resizetreeviews(struct topview *tv)
{
    int height;
    int widthl;
    int widthr;

    height = LINES - 3;
    widthl = ceil(1.0 * COLS / 2);
    widthr = floor(1.0 * COLS / 2);

    if (tv->dira.tree.win)
        delwin(tv->dira.tree.win);
    if (tv->dirb.tree.win)
        delwin(tv->dirb.tree.win);

    tv->dira.tree.win = newwin(height, widthl, 0, 0);
    tv->dirb.tree.win = newwin(height, widthr, 0, widthl);
}

static void
resizelineviews(struct topview *tv)
{
    tv->dira.pick.win = newwin(1, COLS, (LINES - 2 - 1), 0);
    tv->dirb.pick.win = newwin(1, COLS, (LINES - 1 - 1), 0);
    tv->cmd.win       = newwin(1, COLS, (LINES - 0 - 1), 0);
}

static void
initwindows(struct topview *tv)
{
    resizelineviews(tv);
    resizetreeviews(tv);
    tv->dira.tree.focus = 1;
    tv->dira.depth = 1;
    tv->dirb.depth = 2;
}

static void
initbuffers(struct topview *tv)
{
    tv->dira.cwd = getcwd(0, 0);
    tv->dirb.cwd = getcwd(0, 0);
    readpick(&tv->dira, "A");
    readpick(&tv->dirb, "B");
    readtree(&tv->dira);
    readtree(&tv->dirb);
}

static void
writepick(struct dirview *dv, char id)
{
    char txt[1000] = {0};

    werase(dv->pick.win);
    snprintf(txt, 1000, "%c = %s", id, dv->pick.txt);
    mvwprintw(dv->pick.win, 0, 0, txt);
}

static struct line *
getlineat(struct line *l, int n)
{
    int          i;

    for (i = 0; l && (i < n); i++)
        l = l->next;

    return l;
}

static void
updatelinetop(struct paneview *pane)
{
    int span;
    int maxy;
    int spill;

    span = pane->pos - pane->top;
    maxy = getmaxy(pane->win);
    if (span >= maxy) {
        spill = span - maxy;
        pane->top += spill + 1;
    }
    if (pane->pos < pane->top)
        pane->top = pane->pos;
}

static void
writetree(struct paneview *tree)
{
    struct line *l;
    int          height;
    int          i;

    if (!tree)
        return;
    updatelinetop(tree);
    l = getlineat(tree->lines, tree->top);
    if (!l)
        return;

    werase(tree->win);
    height = getmaxy(tree->win);
    for (i = 0; l && (i < height); i++) {
        mvwprintw(tree->win, i, 0, "%s\n", l->txt);
        l = l->next;
    }
}

static void
writecmd(struct lineview *cmd)
{
    werase(cmd->win);
    mvwprintw(cmd->win, 0, 0, "$ ");
    mvwprintw(cmd->win, 0, 2, cmd->txt);
}

static void
highlightselection(struct topview *tv)
{
    int line;

    if (tv->dira.tree.focus) {
        line = tv->dira.tree.pos - tv->dira.tree.top;
        mvwchgat(tv->dira.tree.win, line, 0, -1, A_REVERSE, 0, 0);
    } else if (tv->dirb.tree.focus) {
        line = tv->dirb.tree.pos - tv->dirb.tree.top;
        mvwchgat(tv->dirb.tree.win, line, 0, -1, A_REVERSE, 0, 0);
    } else {
        mvwchgat(tv->cmd.win, 0, (2 + tv->cmd.pos), 1, A_REVERSE, 0, 0);
    }
}

static void
updateviews(struct topview *tv)
{
    writepick(&tv->dira, 'A');
    writepick(&tv->dirb, 'B');
    writetree(&tv->dira.tree);
    writetree(&tv->dirb.tree);
    writecmd(&tv->cmd);

    highlightselection(tv);

    wrefresh(tv->dira.tree.win);
    wrefresh(tv->dirb.tree.win);
    wrefresh(tv->dira.pick.win);
    wrefresh(tv->dirb.pick.win);
    wrefresh(tv->cmd.win);
}

static void
switchfocus(struct topview *tv)
{
    int *fa;
    int *fb;

    fa = &tv->dira.tree.focus;
    fb = &tv->dirb.tree.focus;

    if (*fa) {
        *fa = 0;
        *fb = 1;
    } else if (*fb) {
        *fa = 0;
        *fb = 0;
    } else {
        *fa = 1;
        *fb = 0;
    }
}

static void
updateposition(struct paneview *p, int step)
{
    p->pos += step;

    if (p->pos < 0)
        p->pos = 0;

    if (p->pos > p->end)
        p->pos = p->end;
}

static struct dirview *
getfocuseddir(struct topview *tv)
{
    if (tv->dira.tree.focus)
        return &tv->dira;
    else if (tv->dirb.tree.focus)
        return &tv->dirb;
    else
        return 0;
}

static void
moveselection(struct topview *tv, int step)
{
    struct dirview *d;

    d = getfocuseddir(tv);
    if (!d)
        return;

    updateposition(&d->tree, step);
}

static size_t
matchlen(char *str, char *hay)
{
    size_t i;

    for (i = 0; i < strlen(hay); i++)
        if (str[i] != hay[i])
            break;

    /* TODO string safely */
    return i;
}

static ssize_t
getposfromentry(struct dirview *d, char *path)
{
    ssize_t  i;
    ssize_t  pos;
    ssize_t  match;
    ssize_t  new;
    char     buf[BUFSIZ];
    FILE    *fp;

    pos = 0;
    match = 0;
    fp = popentree(d->cwd, 1, d->depth);
    for (i = 0; fgets(buf, (sizeof(buf) - 1), fp); i++) {
        strtok(buf, "\n");
        new = matchlen(path, buf);
        if (new > match) {
            match = new;
            pos = i;
        }
    }
    pclose(fp);

    return pos;
}

static void
cdup(struct topview *tv)
{
    struct dirview *d;
    char            buf[BUFSIZ] = {0};
    FILE           *fp;

    d = getfocuseddir(tv);
    if (!d)
        return;

    snprintf(buf, (sizeof(buf) - 1), "sh -c 'cd %s/..; pwd'", d->cwd);
    fp = popen(buf, "r");
    fgets(buf, (sizeof(buf) - 1), fp);
    strtok(buf, "\n");

    if (pclose(fp) == 0) {
        d->cwd = strdup(buf);
        readtree(d);
        d->tree.pos = getposfromentry(d, d->pick.txt);
    }
}

static char *
pick1up(struct dirview *d)
{
    char *targ;
    int   skip;

    targ = strdup(d->pick.txt);
    strtok(targ, "\n");

    skip = strlen(d->cwd);
    strtok(&targ[skip], "/");

    return targ;
}

static void
cddown(struct topview *tv)
{
    struct dirview *d;
    char           *targ;

    d = getfocuseddir(tv);
    if (!d)
        return;

    targ = pick1up(d);
    d->cwd = strdup(targ);
    readtree(d);
    d->tree.pos = getposfromentry(d, d->pick.txt);
    /* TODO free resources */
}

static void
nudgedepth(struct topview *tv, int n)
{
    struct dirview *d;

    d = getfocuseddir(tv);
    if (!d)
        return;
    if (d->depth + n < 1)
        return;

    d->depth += 1;
    readtree(d);
    d->tree.pos = getposfromentry(d, d->pick.txt);
}

static void
handleselection(struct topview *tv, int c)
{
    if (0) {
    } else if (c == 'j') {
        moveselection(tv, 1);
    } else if (c == 'J') {
        moveselection(tv, 5);
    } else if (c == 'k') {
        moveselection(tv, -1);
    } else if (c == 'K') {
        moveselection(tv, -5);
    } else if (c == 'h') {
        cdup(tv);
    } else if (c == 'H') {
        cdup(tv);
        nudgedepth(tv, 1);
    } else if (c == 'l') {
        cddown(tv);
    } else if (c == 'L') {
        nudgedepth(tv, 1);
    }

    readpick(&tv->dira, "A");
    readpick(&tv->dirb, "B");
}

static void
handlebackspace(struct lineview *cmd)
{
    if (cmd->pos <= 0)
        return;

    cmd->pos--;
    cmd->txt[cmd->pos] = 0;
}

static void
handleenter(struct topview *tv)
{
    struct lineview *cmd;
    char             str[BUFSIZ] = {0};

    cmd = &tv->cmd;

    snprintf(str, (sizeof(str) - 1), "%s >/dev/null 2>&1", cmd->txt);
    system(str);

    memset(cmd->txt, 0, sizeof(cmd->txt));
    cmd->pos = 0;
    readtree(&tv->dira);
    readtree(&tv->dirb);
}

static void
handlewriting(struct topview *tv, int c)
{
    struct lineview *cmd;

    cmd = &tv->cmd;
    if (!cmd || (cmd->pos >= (ssize_t)sizeof(cmd->txt)))
        return;

    if (c == KEY_BACKSPACE) {
        handlebackspace(cmd);
        return;
    } else if ((c == '\n') || (c == '\r')) {
        handleenter(tv);
        return;
    }

    if (!isprint(c))
        return;
    cmd->txt[cmd->pos] = c;
    cmd->pos++;
}

static void
handleinput(struct topview *tv, int c)
{
    if (c == '\t') {
        switchfocus(tv);
        return;
    }

    if (tv->dira.tree.focus || tv->dirb.tree.focus)
        handleselection(tv, c);
    else
        handlewriting(tv, c);
}

static void
initcurses(void)
{
    setlocale(LC_ALL, "");

    initscr();
    atexit((void (*)(void))endwin);
    wrefresh(stdscr);

    cbreak();
    noecho();
    nonl();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    curs_set(0);
}

int
main(void)
{
    struct topview tv = {0};
    int c;

    initcurses();
    initwindows(&tv);
    initbuffers(&tv);

    for (;;) {
        updateviews(&tv);

        c = getch();
        if (c == 'q')
            break;
        if (c == KEY_RESIZE) {
            wnoutrefresh(stdscr);
            resizelineviews(&tv);
            resizetreeviews(&tv);
        }
        handleinput(&tv, c);
    }

    return EXIT_SUCCESS;
}
