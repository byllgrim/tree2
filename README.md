# tree2

A filebrowser.

`curses` for graphics.
`tree` for directory view.
Environment variables for selection.
Shell for executing commands.



# Status

Proof of concept is working!

Still many bugs exist, and some features are missing.
See issue tracker https://gitlab.com/byllgrim/tree2/issues



# Usage

There are 2 panes showing outputs of `$ tree` for their respective `cwd`.

There are 2 lines `A = foo` and `B = bar` showing the full path of the
selected directory entry in either of the panes.
These lines set environment variables `A` and `B` so they can be used
in command incantations.

A final line at the bottom is for command entry.
E.g. `$ cp $A $B/newfile`.

    j/k   - Move selection down/up
    l/h   - Navigate in/out
    TAB   - Switch between directory views and command view
    q     - quit
    ENTER - Execute command while in command view



# Motivation

I have a big document where each chapter and section is represented by
each their own directory.
Now I wanna move some texts quickly from one section to another, but then
I need to have an overview of source and destination, and a quick way to
select and refer to selection.
But I still wanted the power of the terminal tools like `mkdir`, etc.

The closest existing thing I could find was
[lf](https://github.com/gokcehan/lf).
And I also considered e.g. some file managers that
[rocks](https://suckless.org/rocks/).
mc ain't quite good either, because I wanna see more context at once,
and I don't care for file metadata and all the mc bloat.
